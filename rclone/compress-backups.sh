#!/bin/bash

RCLONE_CONFIG=/root/.config/rclone/rclone.conf
BACKUP_TIME=$(date +'%Y%m%d%H%M')
BACKUP_FILE="worlds-$BACKUP_TIME.tar.gz"

export RCLONE_CONFIG

tar -czvf /minecraft-backups/world.tar.gz /minecraft/world
tar -czvf /minecraft-backups/world_nether.tar.gz /minecraft/world_nether
tar -czvf /minecraft-backups/world_skylands.tar.gz /minecraft/world_skylands
tar -czvf /minecraft-backups/world_the_end.tar.gz /minecraft/world_the_end


tar -czvf /minecraft-worlds-backups/$BACKUP_FILE /minecraft-backups

rm -rf /minecraft-backups/*

echo "Copy to google"

/usr/sbin/rclone copy /minecraft-worlds-backups/$BACKUP_FILE google:/minecraft/backups -vv

echo "Copy to google: done"

rm -rf /minecraft-worlds-backups/*

echo "Directory '/minecraft-worlds-backups/' cleaned"