## MineCraft Server(Spigot 1.15.2) based  on Docker

Connect to runned server:

    docker exec -ti minecraft_1152_main_1 screen -r minecraft
    
Exit from server console command:

Press `Ctrl + A` after that `Ctrl + D`

`IMPORTANT`: If you are will press `Ctrl + C` you will down server

### Run server when docker containers already up

    docker-compose kill && docker-compose up -d
